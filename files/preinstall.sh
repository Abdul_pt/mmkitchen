#!/system/bin/sh

if [ -e /system/preinstall/ ]; then
    if [ ! -e data/local/tmp/.preinstall ]; then
	busybox touch data/local/tmp/.preinstall
	APKLIST=`ls /system/preinstall/*.apk`
	for INFILES in $APKLIST; do
	    pm install -r $INFILES
	done
    fi
fi
exit

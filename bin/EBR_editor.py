#!/usr/bin/python
#
#
#   EBR_Editor www.freaktab.com
#
#   Copyright 2014 Brian Mahoney brian@mahoneybrian.wanadoo.co.uk
#
#   <version>1.0.0</version>
#
############################################################################
#
#   EBR_Editor is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   FreakTabKitchen is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with FreakTabKitchen.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
import os


#consts
systemEBROffset = 0x1C6
EBR2Offset = 0x1C6
cacheEBROffset = 0x1D6
dataEBROffset = 0x1E6
ebr2PointerOffset = 0x1F6




newDataSizeGB = 4


class EBR:
    '''Encapsulate EBR handling'''
    _ROMFolder = ''
    _EBR1Bytes = []
    _EBR2Bytes = []


    #_Bbytes = []
    _A = 0  #A
    _B = 0  #B
    _C = 0  #C
    _D = 0  #D
    _E = 0  #E


    def __init__(self,ROMFolder):
        self._ROMFolder=ROMFolder

        EBR1 = os.path.join(self._ROMFolder,'EBR1')
        EBR2 = os.path.join(self._ROMFolder,'EBR2')


        with open(EBR1, 'rb') as fr:
            self._EBR1Bytes = fr.read()

        with open(EBR2, 'rb') as fr:
            self._EBR2Bytes = fr.read()


        self._A = self._getval(self._EBR1Bytes,dataEBROffset)       #A
        self._B = self._getval(self._EBR1Bytes,dataEBROffset + 4)   #B
        self._C = self._getval(self._EBR1Bytes,ebr2PointerOffset)   #C


        self._D = self._getval(self._EBR2Bytes,EBR2Offset)          #D
        self._E = self._getval(self._EBR2Bytes,EBR2Offset + 4)      #E


    def datasize(self):
        return self._B


    def setdatasize(self, size):
        self._B = size/512
        self._D = (self._A + self._B) - self._C
        self._E = 0xffffffff - (self._A + self._B)
        self._EBR1Bytes = EBR._updateEBR(self._EBR1Bytes, self._B, dataEBROffset + 4)
        self._EBR2Bytes = EBR._updateEBR(self._EBR2Bytes, self._D, EBR2Offset)
        self._EBR2Bytes = EBR._updateEBR(self._EBR2Bytes, self._E, EBR2Offset + 4)


    def writeEBRfiles(self,suffix):
        suffix = str(suffix)
        self.writeEBR1New(suffix)
        self.writeEBR2New(suffix)


    def writeEBR1New(self,suffix):
        ebr = os.path.join(self._ROMFolder, 'EBR1_' + suffix)
        EBR.writeEBR(self._EBR1Bytes, ebr)


    def writeEBR2New(self,suffix):
        ebr = os.path.join(self._ROMFolder, 'EBR2_' + suffix)
        EBR.writeEBR(self._EBR2Bytes, ebr)


    @staticmethod
    def writeEBR(ebrbytes,ebr):
        with open(ebr, 'wb') as fw:
            fw.write(ebrbytes)


    def tailEBR2(self):
        EBR._dumpEBR('\n\nEBR2',self._EBR2Bytes,28)


    def tailEBR1(self):
        EBR._dumpEBR('\n\nEBR1',self._EBR1Bytes,28)


    @staticmethod
    def _updateEBR(EBR, size, offset):
        tval = size
        print size
        hexval = '00000000' + hex(tval)[2:]
        if hexval[-1] == 'L':
            hexval=hexval[:-1]
            print hexval
        hexval = hexval[-8:]
        #print '\n', hexval , hexval[4:6], int(hexval[4:6],16)
        return EBR[:offset ] + chr(int(hexval[6:8],16)) + chr(int(hexval[4:6],16)) + chr(int(hexval[2:4],16)) + chr(int(hexval[:2],16)) + EBR[offset + 4:]


    @staticmethod
    def _dumpEBR(prompt,theebr,rowoffset):
        print prompt
        for r in range(rowoffset,32):
            print '\n',hex(16*r),
            for i in range(0,16):
                s= '00' + hex(int(theebr[16*r + i].encode('hex'),16))[2:]
                print s[-2:],


    @staticmethod
    def _getval(ebr,offset):
        bytes = []
        bytes = [int( ebr[offset].encode('hex'),16)
                 ,int( ebr[offset + 1].encode('hex'),16)
                 ,int( ebr[offset + 2].encode('hex'),16)
                 ,int( ebr[offset + 3].encode('hex'),16)]
        hbytes = [ebr[offset].encode('hex')
                 ,ebr[offset + 1].encode('hex')
                 ,ebr[offset + 2].encode('hex')
                 ,ebr[offset + 3].encode('hex')]
        print hex(offset), hbytes
        return (bytes[0]
            + 256*(bytes[1]
            + 256*(bytes[2]
            + 256 *bytes[3])))


###############################################################################


try:
    fpath = os.environ[ "SOURCEPATH" ]
    nds = raw_input('Enter data size required (Gb): ')


    if fpath == '':
        fpath = '/home/brian/mtkKitchen0.1/ebr'
        fpath = 'd:\ebr'

    if nds != '':
        newDataSizeGB = int(nds)


    ebr = EBR(fpath)


    ebr.tailEBR1()
    ebr.tailEBR2()


    ebr.setdatasize(newDataSizeGB*1024*1024*1024)


    ebr.tailEBR1()
    ebr.tailEBR2()

    ebr.writeEBRfiles(str(newDataSizeGB) + 'Gb')


except Exception as e:
    print 'ebr::main '
    print e
